# TM-SE-14

	TM-SE-14 is a program project manager

# TECHNOLOGY STACK

	Maven 4.0
	Java SE 1.8
	Junit 4.11


# DEVELOPER

	Habibrahmanov Ilyas
	habthemes@gmail.com

# BUILDING FROM SOURCE

	mvn install

# SOFTWARE REQUIREMENTS

	jdk 1.8


# USING THE PROJECT MANAGER

	From the command-line
	Download the project manager and run it with:
	java -jar D:\Habibrahmanov\tm-se-14\target\tm-se-14-1.0-SNAPSHOT.jar