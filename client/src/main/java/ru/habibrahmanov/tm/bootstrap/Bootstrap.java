package ru.habibrahmanov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.habibrahmanov.tm.api.*;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.endpoint.ProjectEndpointService;
import ru.habibrahmanov.tm.endpoint.SessionEndpointService;
import ru.habibrahmanov.tm.endpoint.TaskEndpointService;
import ru.habibrahmanov.tm.endpoint.UserEndpointService;
import ru.habibrahmanov.tm.endpoint.DomainEndpointService;

import java.util.*;

public class Bootstrap implements ServiceLocator {
    @NotNull private final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();
    @NotNull private final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
    @NotNull private final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();
    @NotNull private final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();
    @NotNull private final IDomainEndpoint domainEndpoint = new DomainEndpointService().getDomainEndpointPort();
    @NotNull private SessionDto currentSession;
    @NotNull private final Scanner scanner = new Scanner(System.in);
    @NotNull private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    private final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.habibrahmanov.tm.command").getSubTypesOf(AbstractCommand.class);


    public void init() throws Exception {
        registry(classes);
        start();
    }

    public void registry(@Nullable final Set<Class<? extends AbstractCommand>> classes) throws Exception {
        if (classes == null) return;
        for (Class clazz : classes) {
            registry(clazz);
        }

//            userEndpoint.registryAdmin("admin", "admin", "admin");
    }

    public void registry(@NotNull final Class clazz) throws IllegalAccessException, InstantiationException {
        @NotNull final AbstractCommand abstractCommand = (AbstractCommand) clazz.newInstance();
        abstractCommand.setBootstrap(this);
        @Nullable final String nameCommand = abstractCommand.getName();
        @Nullable final String descriptionCommand = abstractCommand.getDescription();
        if (nameCommand == null || nameCommand.isEmpty()) return;
        if (descriptionCommand == null || descriptionCommand.isEmpty()) return;
        commands.put(nameCommand, abstractCommand);
    }

    public void start() throws Exception {
        System.out.println("***WELCOME TO TASK MANAGER***");
        System.out.println("Enter \"help\" to show all command.");
        String command = "";
        while (!command.equals("exit")) {
                command = scanner.nextLine();
                execute(command);
                System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        }
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        try {
            abstractCommand.execute();
        } catch (Exception e) {
//            System.err.println(e.getMessage());
            e.printStackTrace();
        }

    }

    @Override
    public TreeMap<String, AbstractCommand> sortCommands() {
        return new TreeMap<>(commands);
    }

    @Override
    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    @Override
    public Scanner getScanner() {
        return scanner;
    }

    @Override
    public IProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @Override
    public ISessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    @Override
    public ITaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @Override
    public IUserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    @Override
    public SessionDto getCurrentSession() {
        return currentSession;
    }

    @Override
    public IDomainEndpoint getDomainEndpoint() {
        return domainEndpoint;
    }

    @Override
    public void setCurrentSession(SessionDto currentSession) {
        this.currentSession = currentSession;
    }
}
