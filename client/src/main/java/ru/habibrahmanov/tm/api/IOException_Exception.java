
package ru.habibrahmanov.tm.api;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.3.5
 * 2020-02-09T23:31:48.386+03:00
 * Generated source version: 3.3.5
 */

@WebFault(name = "IOException", targetNamespace = "http://api.tm.habibrahmanov.ru/")
public class IOException_Exception extends Exception {

    private ru.habibrahmanov.tm.api.IOException ioException;

    public IOException_Exception() {
        super();
    }

    public IOException_Exception(String message) {
        super(message);
    }

    public IOException_Exception(String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public IOException_Exception(String message, ru.habibrahmanov.tm.api.IOException ioException) {
        super(message);
        this.ioException = ioException;
    }

    public IOException_Exception(String message, ru.habibrahmanov.tm.api.IOException ioException, java.lang.Throwable cause) {
        super(message, cause);
        this.ioException = ioException;
    }

    public ru.habibrahmanov.tm.api.IOException getFaultInfo() {
        return this.ioException;
    }
}
