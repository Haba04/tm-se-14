package ru.habibrahmanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.api.TaskDto;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.api.Task;

import java.util.List;

public class TaskFindAllSortedByDateEndCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-sort-data-end";
    }

    @Override
    public String getDescription() {
        return "task find all sorted by data end command";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND ALL TASKS SORTED BY DATE END]");
        @NotNull final List<TaskDto> taskList = serviceLocator.getTaskEndpoint().findAllSortedByDateEndTask(serviceLocator.getCurrentSession());
        for (TaskDto task : taskList) {
            System.out.println(task);
        }
    }
}
