package ru.habibrahmanov.tm.command.data.json;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class SystemLoadJaxbJsonCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "load-jaxb-json";
    }

    @Override
    public String getDescription() {
        return "loading the subject area using externalization and jaxb technology in json transport format";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getDomainEndpoint().loadJaxbJson(serviceLocator.getCurrentSession());
        System.out.println("OK");
    }
}
