package ru.habibrahmanov.tm.command;

import ru.habibrahmanov.tm.api.ServiceLocator;
import java.text.SimpleDateFormat;

public abstract class AbstractCommand {
        public final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        protected ServiceLocator serviceLocator;
        public void setBootstrap(ServiceLocator serviceLocator) {
            this.serviceLocator = serviceLocator;
        }
        public abstract String getName();
        public abstract String getDescription();
        public abstract void execute() throws Exception;
}

