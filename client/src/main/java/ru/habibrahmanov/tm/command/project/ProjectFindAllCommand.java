package ru.habibrahmanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.api.Project;
import ru.habibrahmanov.tm.api.ProjectDto;
import ru.habibrahmanov.tm.command.AbstractCommand;

import java.util.List;

public final class ProjectFindAllCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project-find-all";
    }

    @Override
    public String getDescription() {
        return "show all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND ALL PROJECTS]");
        @NotNull final List<ProjectDto> projectList = serviceLocator.getProjectEndpoint().findAllProject(serviceLocator.getCurrentSession());
        for (ProjectDto project : projectList) {
            System.out.println("PROJECT ID: " + project.getId());
            System.out.println("NAME: " + project.getName() + ", DESCRIPTION: " + project.getDescription());
            System.out.println("DATE BEGIN: " + project.getDateBegin() + ", DATE END: " + project.getDateEnd());
            System.out.println();
        }
    }
}