package ru.habibrahmanov.tm.wrapper;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.dto.ProjectDto;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "projectList")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProjectList {
    @Nullable
    @XmlElement(name = "project")
    private List<ProjectDto> projectList = null;

    @Nullable
    public List<ProjectDto> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<ProjectDto> projectList) {
        this.projectList = projectList;
    }
}
