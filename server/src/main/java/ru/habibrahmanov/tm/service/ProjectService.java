package ru.habibrahmanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.IProjectService;
import ru.habibrahmanov.tm.comporator.ProjectDateBeginComparator;
import ru.habibrahmanov.tm.comporator.ProjectDateEndComparator;
import ru.habibrahmanov.tm.dto.ProjectDto;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.repository.ProjectRepository;
import ru.habibrahmanov.tm.util.HibernateUtil;

import javax.persistence.EntityManager;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class ProjectService extends AbstractService implements IProjectService {

    @Override
    public void insert(
            @Nullable final String name, @Nullable final String description, @Nullable final String dateBegin,
            @Nullable final String dateEnd, @Nullable final String userId
    ) throws IncorrectValueException, ParseException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        if (description == null || description.isEmpty()) throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty()) throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty()) throw new IncorrectValueException();
        @Nullable final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @Nullable final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable final User user = entityManager.find(User.class, userId);
        entityManager.getTransaction().begin();
        projectRepository.persist(new Project(name, description, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd), user));
        entityManager.getTransaction().commit();
    }

    @NotNull
    public ProjectDto findOne(@Nullable final String userId, @Nullable final String projectId) throws IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        @Nullable final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @Nullable final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable final Project project = projectRepository.findOne(userId, projectId);
        return convertEntityToDto(project);
    }

    @Override
    public void persist(@Nullable final Project project) throws IncorrectValueException {
        if (project == null) throw new IncorrectValueException();
        @Nullable final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @Nullable final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.persist(project);
        entityManager.getTransaction().commit();
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll(@Nullable final String userId) throws ListIsEmptyExeption, IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        @Nullable final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @Nullable final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        if (projectRepository.findAllByUserId(userId).isEmpty()) throw new ListIsEmptyExeption();
        @Nullable final List<Project> projectList = projectRepository.findAllByUserId(userId);
        @Nullable final List<ProjectDto> projectDtoList = new ArrayList<>();
        for (Project project : projectList) {
            projectDtoList.add(convertEntityToDto(project));
        }
        return projectDtoList;
    }

    @NotNull
    @Override
    public List<Project> findAll() throws ListIsEmptyExeption {
        @Nullable final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @Nullable final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        if (projectRepository.findAll().isEmpty()) throw new ListIsEmptyExeption();
        return projectRepository.findAll();
    }

    @Override
    public void removeAll(@Nullable final String userId) throws IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        @Nullable final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @Nullable final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.removeAll(userId);
        entityManager.getTransaction().commit();
    }

    @Override
    public void removeOne(@Nullable final String projectId, @Nullable final String userId) throws IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        @Nullable final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @Nullable final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.removeOne(projectId, userId);
        entityManager.getTransaction().commit();
    }

    @Override
    public void update(
            @Nullable final String userId, @Nullable final String projectId, @Nullable final String name,
            @Nullable final String description, @Nullable final String status, @Nullable final String dateBegin, @Nullable final String dateEnd
    ) throws IncorrectValueException, ParseException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        if (description == null || description.isEmpty()) throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty()) throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty()) throw new IncorrectValueException();
        @Nullable final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @Nullable final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable final User user = entityManager.find(User.class, userId);
        entityManager.getTransaction().begin();
        projectRepository.update(new Project(name, description, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd), user));
        entityManager.getTransaction().commit();
    }

    @Override
    public void merge(
            @NotNull final Project project
    ) throws IncorrectValueException, ParseException {
        @Nullable final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @Nullable final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        if (projectRepository.findOne(project.getId(), project.getUser().getId()) == null) {
            persist(project);
            entityManager.getTransaction().commit();
        } else {
            update(project.getUser().getId(), project.getId(), project.getName(), project.getDescription(), project.getStatus().displayName(),
                    dateFormat.format(project.getDateBegin()), dateFormat.format(project.getDateEnd()));
            entityManager.getTransaction().commit();
        }
    }

    @NotNull
    @Override
    public List<ProjectDto> searchByString(
            @Nullable final String userId, @Nullable final String string
    ) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (string == null || string.isEmpty()) throw new IncorrectValueException();
        @Nullable final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @Nullable final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        if (projectRepository.getProjectsByString(userId, string).isEmpty()) throw new ListIsEmptyExeption();
        @Nullable final List<Project> projectList = projectRepository.getProjectsByString(userId, string);
        @Nullable final List<ProjectDto> projectDtoList = new ArrayList<>();
        for (Project project : projectList) {
            projectDtoList.add(convertEntityToDto(project));
        }
        return projectDtoList;
    }

    @Override
    public ProjectDto convertEntityToDto(Project project) {
        ProjectDto projectDto = new ProjectDto();
        projectDto.setId(project.getId());
        projectDto.setName(project.getName());
        projectDto.setDescription(project.getDescription());
        projectDto.setStatus(project.getStatus());
        projectDto.setDateBegin(project.getDateBegin());
        projectDto.setDateEnd(project.getDateEnd());
        projectDto.setUserId(project.getUser().getId());
        return projectDto;
    }

    @Override
    public Project convertDtoToEntity(ProjectDto projectDto) {
        Project project = new Project();
        project.setId(projectDto.getId());
        project.setName(projectDto.getName());
        project.setDescription(projectDto.getDescription());
        project.setStatus(projectDto.getStatus());
        project.setDateBegin(projectDto.getDateBegin());
        project.setDateEnd(projectDto.getDateEnd());
        return project;
    }
}
