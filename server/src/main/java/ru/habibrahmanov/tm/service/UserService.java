package ru.habibrahmanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.IUserService;
import ru.habibrahmanov.tm.dto.ProjectDto;
import ru.habibrahmanov.tm.dto.TaskDto;
import ru.habibrahmanov.tm.dto.UserDto;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.repository.UserRepository;
import ru.habibrahmanov.tm.util.HashUtil;
import ru.habibrahmanov.tm.util.HibernateUtil;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public final class UserService extends AbstractService implements IUserService {

    @Override
    public void registryAdmin(
            @Nullable final String login, @Nullable final String password, @Nullable final String passwordConfirm
    ) throws IllegalArgumentException, IncorrectValueException {
        if (login == null || login.isEmpty()) throw new IncorrectValueException();
        if (password == null || password.isEmpty()) throw new IncorrectValueException();
        if (!password.equals(passwordConfirm)) throw new IllegalArgumentException("PASSWORDS DO NOT MATCH");
        @Nullable final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @Nullable final UserRepository userRepository = new UserRepository(entityManager);
        if (userRepository.findByLogin(login) == null) throw new IllegalArgumentException("SUCH USER EXISTS");
        entityManager.getTransaction().begin();
        userRepository.persist(new User(login, HashUtil.md5(password)));
        entityManager.getTransaction().commit();
    }

    @Override
    public void registryUser(@Nullable final User user) throws IllegalArgumentException {
        if (user == null) throw new IllegalArgumentException();
        user.setPassword(HashUtil.md5(user.getPassword()));
        @Nullable final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @Nullable final UserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.persist(user);
        entityManager.getTransaction().commit();
    }

    @Override
    public void updatePassword(
            @Nullable final String userId, @Nullable final String curPassword, @Nullable final String newPassword,
            @Nullable final String newPasswordConfirm) throws IncorrectValueException {
        @Nullable final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @Nullable final User user = entityManager.find(User.class, userId);
        if (user == null) return;
        if (curPassword == null || curPassword.isEmpty()) throw new IncorrectValueException();
        if (newPassword == null || newPassword.isEmpty()) throw new IncorrectValueException();
        if (newPasswordConfirm == null || newPasswordConfirm.isEmpty()) return;
        if (!user.getPassword().equals(HashUtil.md5(curPassword))) {
            throw new IllegalArgumentException("CURRENT PASSWORD DOES NOT MATCH USER PASSWORD: " + user.getLogin());
        }
        if (!newPassword.equals(newPasswordConfirm)) throw new IllegalArgumentException("PASSWORDS DO NOT MATCH");
        user.setPassword(HashUtil.md5(newPassword));
    }

    @NotNull
    @Override
    public List<UserDto> findAll() throws ListIsEmptyExeption {
        @Nullable final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @Nullable final UserRepository userRepository = new UserRepository(entityManager);
        if (userRepository.findAll().isEmpty()) throw new ListIsEmptyExeption();
        @Nullable final List<User> userList = userRepository.findAll();
        @Nullable final List<UserDto> userDtoList = new ArrayList<>();
        for (User user : userList) {
            userDtoList.add(convertEntityToDto(user));
        }
        return userDtoList;

    }

    @Override
    public void RemoveAll() {
        @Nullable final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @Nullable final UserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.removeAll();
        entityManager.getTransaction().commit();

    }

    @NotNull
    @Override
    public UserDto findOne(@Nullable final String userId) throws IncorrectValueException {
        @Nullable final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @Nullable final UserRepository userRepository = new UserRepository(entityManager);
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        @Nullable final User user = userRepository.findOne(userId);
        @Nullable final UserDto userDto = convertEntityToDto(user);
        return userDto;
    }

    @NotNull
    @Override
    public UserDto findByLogin(@Nullable final String login) throws ListIsEmptyExeption {
        if (login == null || login.isEmpty()) throw new ListIsEmptyExeption();
        @Nullable final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @Nullable final UserRepository userRepository = new UserRepository(entityManager);
        @Nullable final User user = userRepository.findByLogin(login);
        @Nullable final UserDto userDto = convertEntityToDto(user);
        return userDto;

    }

    @Override
    public UserDto convertEntityToDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setLogin(user.getLogin());
        userDto.setPassword(user.getPassword());
        userDto.setRole(user.getRole());
        return userDto;
    }

    @Override
    public User convertDtoToEntity(UserDto userDto) {
        User user = new User();
        user.setId(userDto.getId());
        user.setLogin(userDto.getLogin());
        user.setPassword(userDto.getPassword());
        user.setRole(userDto.getRole());
        return user;
    }
}
