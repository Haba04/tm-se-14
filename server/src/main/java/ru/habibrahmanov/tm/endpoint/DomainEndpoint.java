package ru.habibrahmanov.tm.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.dto.ProjectDto;
import ru.habibrahmanov.tm.dto.SessionDto;
import ru.habibrahmanov.tm.dto.TaskDto;
import ru.habibrahmanov.tm.dto.UserDto;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.entity.Session;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.enumeration.Role;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RoleTypeException;
import ru.habibrahmanov.tm.exeption.SessionIsNotValidException;
import ru.habibrahmanov.tm.wrapper.ProjectList;
import ru.habibrahmanov.tm.wrapper.TaskList;
import ru.habibrahmanov.tm.wrapper.UserList;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.*;
import java.io.*;
import java.text.ParseException;
import java.util.Collection;
import java.util.Iterator;

@NoArgsConstructor
@WebService(endpointInterface = "ru.habibrahmanov.tm.api.IDomainEndpoint")
public class DomainEndpoint extends AbstractEndpoint implements ru.habibrahmanov.tm.api.IDomainEndpoint {

    @Override
    @WebMethod
    public void deserializationDomain(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws IOException, SessionIsNotValidException, ClassNotFoundException, ParseException, IncorrectValueException, RoleTypeException {
        endpointLocator.getSessionService().validate(session);
        if (!endpointLocator.getUserService().findOne(session.getUserId()).getRole().equals(Role.ADMIN))
            throw new RoleTypeException();

        @NotNull final FileInputStream userFileInputStream = new FileInputStream("src\\main\\file\\userFile");
        @NotNull final ObjectInputStream userObjectInputStream = new ObjectInputStream(userFileInputStream);
        @NotNull final Collection<User> userCollection = (Collection<User>) userObjectInputStream.readObject();
        for (User user : userCollection) {
            endpointLocator.getUserEndpoint().registryUser(user);
        }
        userFileInputStream.close();
        userObjectInputStream.close();

        @NotNull final FileInputStream taskFileInputStream = new FileInputStream("src\\main\\file\\taskFile");
        @NotNull final ObjectInputStream taskObjectInputStream = new ObjectInputStream(taskFileInputStream);
        @NotNull final Collection<Task> taskCollection = (Collection<Task>) taskObjectInputStream.readObject();
        for (Task task : taskCollection) {
            endpointLocator.getTaskEndpoint().persistTask(session, task);
        }
        taskFileInputStream.close();
        taskObjectInputStream.close();

        final FileInputStream projectFileInputStream = new FileInputStream("src\\main\\file\\projectFile");
        final ObjectInputStream projectObjectInputStream = new ObjectInputStream(projectFileInputStream);
        @NotNull final Collection<Project> projectCollection = (Collection<Project>) projectObjectInputStream.readObject();
        for (Project project : projectCollection) {
            endpointLocator.getProjectEndpoint().persistProject(session, project);
        }
        projectFileInputStream.close();
        projectObjectInputStream.close();
    }

    @Override
    @WebMethod
    public void serializationDomain(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws IncorrectValueException, ListIsEmptyExeption, SessionIsNotValidException, IOException, RoleTypeException {
        endpointLocator.getSessionService().validate(session);
        if (!endpointLocator.getUserService().findOne(session.getUserId()).getRole().equals(Role.ADMIN))
            throw new RoleTypeException();

        @NotNull final FileOutputStream userFileOutputStream = new FileOutputStream("src\\main\\file\\userFile");
        @NotNull final ObjectOutputStream userObjectOutputStream = new ObjectOutputStream(userFileOutputStream);
        userObjectOutputStream.writeObject(endpointLocator.getUserEndpoint().findAll(session));
        userFileOutputStream.close();
        userObjectOutputStream.close();

        @NotNull final FileOutputStream taskFileOutputStream = new FileOutputStream("src\\main\\file\\taskFile");
        @NotNull final ObjectOutputStream taskObjectOutputStream = new ObjectOutputStream(taskFileOutputStream);
        taskObjectOutputStream.writeObject(endpointLocator.getTaskEndpoint().findAllTask(session));
        taskFileOutputStream.close();
        taskObjectOutputStream.close();

        @NotNull final FileOutputStream projectFileOutputStream = new FileOutputStream("src\\main\\file\\projectFile");
        @NotNull final ObjectOutputStream projectObjectOutputStream = new ObjectOutputStream(projectFileOutputStream);
        projectObjectOutputStream.writeObject(endpointLocator.getProjectEndpoint().findAllProject(session));
        projectFileOutputStream.close();
        projectObjectOutputStream.close();
    }

    @Override
    @WebMethod
    public void loadJacksonJson(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws IOException, ParseException, IncorrectValueException, SessionIsNotValidException, RoleTypeException {
        endpointLocator.getSessionService().validate(session);
        if (!endpointLocator.getUserService().findOne(session.getUserId()).getRole().equals(Role.ADMIN))
            throw new RoleTypeException();

        @NotNull final ObjectMapper userMapper = new ObjectMapper();
        @NotNull final UserList userList = userMapper.readValue(new File("src\\main\\file\\userJackson.json"), UserList.class);
        for (UserDto user : userList.getUserList()) {
            endpointLocator.getUserEndpoint().registryUser(endpointLocator.getUserService().convertDtoToEntity(user));
        }

        @NotNull final ObjectMapper projectMapper = new ObjectMapper();
        @NotNull final ProjectList projectList = projectMapper.readValue(new File("src\\main\\file\\projectJackson.json"), ProjectList.class);
        for (ProjectDto project : projectList.getProjectList()) {
            endpointLocator.getProjectEndpoint().persistProject(session, endpointLocator.getProjectService().convertDtoToEntity(project));
        }

        @NotNull final ObjectMapper taskMapper = new ObjectMapper();
        @NotNull final TaskList taskList = taskMapper.readValue(new File("src\\main\\file\\taskJackson.json"), TaskList.class);
        for (TaskDto task : taskList.getTaskList()) {
            endpointLocator.getTaskEndpoint().persistTask(session, endpointLocator.getTaskService().convertDtoToEntity(task));
        }
    }

    @Override
    @WebMethod
    public void loadJaxbJson(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws JAXBException, ParseException, IncorrectValueException, SessionIsNotValidException, RoleTypeException {
        endpointLocator.getSessionService().validate(session);
        if (!endpointLocator.getUserService().findOne(session.getUserId()).getRole().equals(Role.ADMIN))
            throw new RoleTypeException();

        @NotNull final JAXBContext projectJaxbContext = JAXBContext.newInstance(ProjectList.class);
        @NotNull final Unmarshaller projectUnmarshaller = projectJaxbContext.createUnmarshaller();
        projectUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        projectUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, Boolean.TRUE);
        @NotNull final ProjectList projectList = (ProjectList) projectUnmarshaller
                .unmarshal(new File("src\\main\\file\\projectList.json"));
        for (ProjectDto project : projectList.getProjectList()) {
            endpointLocator.getProjectEndpoint().persistProject(session, endpointLocator.getProjectService().convertDtoToEntity(project));
        }

        @NotNull final JAXBContext taskJaxbContext = JAXBContext.newInstance(TaskList.class);
        @NotNull final Unmarshaller taskUnmarshaller = taskJaxbContext.createUnmarshaller();
        taskUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        taskUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, Boolean.TRUE);
        @NotNull final TaskList taskList = (TaskList) taskUnmarshaller
                .unmarshal(new File("src\\main\\file\\taskList.json"));
        for (TaskDto task : taskList.getTaskList()) {
            endpointLocator.getTaskEndpoint().persistTask(session, endpointLocator.getTaskService().convertDtoToEntity(task));
        }

        @NotNull final JAXBContext userJaxbContext = JAXBContext.newInstance(UserList.class);
        @NotNull final Unmarshaller userUnmarshaller = userJaxbContext.createUnmarshaller();
        userUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        userUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, Boolean.TRUE);
        @NotNull final UserList userList = (UserList) userUnmarshaller
                .unmarshal(new File("src\\main\\file\\userList.json"));
        for (UserDto user : userList.getUserList()) {
            endpointLocator.getUserEndpoint().registryUser(endpointLocator.getUserService().convertDtoToEntity(user));
        }
    }

    @Override
    @WebMethod
    public void saveJacksonJson(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws SessionIsNotValidException, ListIsEmptyExeption, IOException, IncorrectValueException, RoleTypeException {
        endpointLocator.getSessionService().validate(session);
        if (!endpointLocator.getUserService().findOne(session.getUserId()).getRole().equals(Role.ADMIN))
            throw new RoleTypeException();

        @NotNull final ObjectMapper userMapper = new ObjectMapper();
        @NotNull final UserList userList = new UserList();
        userList.setUserList(endpointLocator.getUserEndpoint().findAll(session));
        userMapper.writerWithDefaultPrettyPrinter().writeValue(new File("src\\main\\file\\userJackson.json"), userList);

        @NotNull final ObjectMapper projectMapper = new ObjectMapper();
        @NotNull final ProjectList projectList = new ProjectList();
        projectList.setProjectList(endpointLocator.getProjectEndpoint().findAllProject(session));
        projectMapper.writerWithDefaultPrettyPrinter().writeValue(new File("src\\main\\file\\projectJackson.json"), projectList);

        @NotNull final ObjectMapper taskMapper = new ObjectMapper();
        @NotNull final TaskList taskList = new TaskList();
        taskList.setTaskList(endpointLocator.getTaskEndpoint().findAllTask(session));
        taskMapper.writerWithDefaultPrettyPrinter().writeValue(new File("src\\main\\file\\taskJackson.json"), taskList);
    }

    @Override
    @WebMethod
    public void saveJaxbJson(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws JAXBException, IncorrectValueException, ListIsEmptyExeption, SessionIsNotValidException, RoleTypeException {
        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        endpointLocator.getSessionService().validate(session);
        if (!endpointLocator.getUserService().findOne(session.getUserId()).getRole().equals(Role.ADMIN))
            throw new RoleTypeException();

        @NotNull final JAXBContext projectJaxbContext = JAXBContext.newInstance(ProjectList.class);
        @NotNull final Marshaller projectMarshaller = projectJaxbContext.createMarshaller();
        projectMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        projectMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        projectMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        @NotNull final ProjectList projectList = new ProjectList();
        projectList.setProjectList(endpointLocator.getProjectEndpoint().findAllProject(session));
        projectMarshaller.marshal(projectList, new File("./projectList.json"));

        @NotNull final JAXBContext taskJaxbContext = JAXBContext.newInstance(TaskList.class);
        @NotNull final Marshaller taskMarshaller = taskJaxbContext.createMarshaller();
        taskMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        taskMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, Boolean.TRUE);
        taskMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        @NotNull final TaskList taskList = new TaskList();
        taskList.setTaskList(endpointLocator.getTaskEndpoint().findAllTask(session));
        taskMarshaller.marshal(taskList, new File("./taskList.json"));

        @NotNull final JAXBContext userJaxbContext = JAXBContext.newInstance(UserList.class);
        @NotNull final Marshaller userMarshaller = userJaxbContext.createMarshaller();
        userMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        userMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, Boolean.TRUE);
        userMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        @NotNull final UserList userList = new UserList();
        userList.setUserList(endpointLocator.getUserEndpoint().findAll(session));
        userMarshaller.marshal(userList, new File("./userList.json"));
    }

    @Override
    @WebMethod
    public void loadJacksonXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws IOException, ParseException, IncorrectValueException, SessionIsNotValidException, RoleTypeException {
        endpointLocator.getSessionService().validate(session);
        if (!endpointLocator.getUserService().findOne(session.getUserId()).getRole().equals(Role.ADMIN))
            throw new RoleTypeException();

        @NotNull final JacksonXmlModule jacksonXmlModule = new JacksonXmlModule();
        jacksonXmlModule.setDefaultUseWrapper(false);
        @NotNull final XmlMapper xmlMapper = new XmlMapper(jacksonXmlModule);

        @NotNull final Iterator<Project> projectIterator = xmlMapper.readerFor(Project.class).readValues(new File("src\\main\\file\\projectJackson.xml"));
        while (projectIterator.hasNext()) {
            endpointLocator.getProjectEndpoint().persistProject(session, projectIterator.next());
        }

        @NotNull final Iterator<Task> taskIterator = xmlMapper.readerFor(Task.class).readValues(new File("src\\main\\file\\taskJackson.xml"));
        while (taskIterator.hasNext()) {
            endpointLocator.getTaskEndpoint().persistTask(session, taskIterator.next());
        }

        @NotNull final Iterator<User> userIterator = xmlMapper.readerFor(User.class).readValues(new File("src\\main\\file\\userJackson.xml"));
        while (userIterator.hasNext()) {
            endpointLocator.getUserEndpoint().registryUser(userIterator.next());
        }
    }

    @Override
    @WebMethod
    public void loadJaxbXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws ParseException, IncorrectValueException, SessionIsNotValidException, JAXBException, RoleTypeException {
        endpointLocator.getSessionService().validate(session);
        if (!endpointLocator.getUserService().findOne(session.getUserId()).getRole().equals(Role.ADMIN))
            throw new RoleTypeException();

        @NotNull final JAXBContext projectJaxbContext = JAXBContext.newInstance(ProjectList.class);
        @NotNull final Unmarshaller projectUnmarshaller = projectJaxbContext.createUnmarshaller();
        @NotNull final ProjectList projectList = (ProjectList) projectUnmarshaller.unmarshal(new File("src\\main\\file\\projectList.xml"));
        for (ProjectDto project : projectList.getProjectList()) {
            endpointLocator.getProjectEndpoint().persistProject(session, endpointLocator.getProjectService().convertDtoToEntity(project));
        }

        @NotNull final JAXBContext taskJaxbContext = JAXBContext.newInstance(TaskList.class);
        @NotNull final Unmarshaller taskUnmarshaller = taskJaxbContext.createUnmarshaller();
        @NotNull final TaskList taskList = (TaskList) taskUnmarshaller.unmarshal(new File("src\\main\\file\\taskList.xml"));
        for (TaskDto task : taskList.getTaskList()) {
            endpointLocator.getTaskEndpoint().persistTask(session, endpointLocator.getTaskService().convertDtoToEntity(task));
        }

        @NotNull final JAXBContext userJaxbContext = JAXBContext.newInstance(UserList.class);
        @NotNull final Unmarshaller userUnmarshaller = userJaxbContext.createUnmarshaller();
        @NotNull final UserList userList = (UserList) userUnmarshaller.unmarshal(new File("src\\main\\file\\userList.xml"));
        for (UserDto user : userList.getUserList()) {
            endpointLocator.getUserEndpoint().registryUser(endpointLocator.getUserService().convertDtoToEntity(user));
        }
    }

    @Override
    @WebMethod
    public void saveJacksonXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws SessionIsNotValidException, IOException, IncorrectValueException, ListIsEmptyExeption, RoleTypeException {
        endpointLocator.getSessionService().validate(session);
        if (!endpointLocator.getUserService().findOne(session.getUserId()).getRole().equals(Role.ADMIN))
            throw new RoleTypeException();

        @NotNull final JacksonXmlModule module = new JacksonXmlModule();
        module.setDefaultUseWrapper(false);
        @NotNull final XmlMapper xmlMapper = new XmlMapper(module);

        @NotNull final FileOutputStream projectFileOutputStream = new FileOutputStream("src\\main\\file\\projectJackson.xml");
        @NotNull final String projectJackson = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(endpointLocator.getProjectService().findAll());
        projectFileOutputStream.write(projectJackson.getBytes());

        @NotNull final FileOutputStream taskFileOutputStream = new FileOutputStream("src\\main\\file\\taskJackson.xml");
        @NotNull final String taskJackson = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(endpointLocator.getTaskService().findAll());
        taskFileOutputStream.write(taskJackson.getBytes());

        @NotNull FileOutputStream userFileOutputStream = new FileOutputStream("src\\main\\file\\userJackson.xml");
        @NotNull final String userJackson = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(endpointLocator.getUserService().findAll());
        userFileOutputStream.write(userJackson.getBytes());
    }

    @Override
    @WebMethod
    public void SaveJaxbXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws JAXBException, IncorrectValueException, ListIsEmptyExeption, SessionIsNotValidException, RoleTypeException {
        endpointLocator.getSessionService().validate(session);
        if (!endpointLocator.getUserService().findOne(session.getUserId()).getRole().equals(Role.ADMIN))
            throw new RoleTypeException();

        @NotNull final ProjectList projectList = new ProjectList();
        @NotNull final TaskList taskList = new TaskList();
        @NotNull final UserList userList = new UserList();

        @NotNull final JAXBContext projectJaxbContext = JAXBContext.newInstance(ProjectList.class);
        @NotNull final Marshaller projectMarshaller = projectJaxbContext.createMarshaller();
        projectMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        projectList.setProjectList(endpointLocator.getProjectEndpoint().findAllProject(session));
        projectMarshaller.marshal(projectList, new File("src\\main\\file\\projectList.xml"));

        @NotNull final JAXBContext taskJaxbContext = JAXBContext.newInstance(TaskList.class);
        @NotNull final Marshaller taskMarshaller = taskJaxbContext.createMarshaller();
        taskMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        taskList.setTaskList(endpointLocator.getTaskEndpoint().findAllTask(session));
        taskMarshaller.marshal(taskList, new File("src\\main\\file\\taskList.xml"));

        @NotNull final JAXBContext userJaxbContext = JAXBContext.newInstance(UserList.class);
        @NotNull final Marshaller userMarshaller = userJaxbContext.createMarshaller();
        userMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        userList.setUserList(endpointLocator.getUserEndpoint().findAll(session));
        userMarshaller.marshal(userList, new File("src\\main\\file\\userList.xml"));
    }
}
