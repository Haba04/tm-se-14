package ru.habibrahmanov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.IProjectEndpoint;
import ru.habibrahmanov.tm.dto.ProjectDto;
import ru.habibrahmanov.tm.dto.SessionDto;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.entity.Session;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RemoveFailedException;
import ru.habibrahmanov.tm.exeption.SessionIsNotValidException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.text.ParseException;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.habibrahmanov.tm.api.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @Override
    @WebMethod
    public void insertProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description,
            @WebParam(name = "dateBegin", partName = "dateBegin") @Nullable final String dateBegin,
            @WebParam(name = "dateEnd", partName = "dateEnd") @Nullable final String dateEnd
    ) throws ParseException, IncorrectValueException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        endpointLocator.getProjectService().insert(name, description, dateBegin, dateEnd, session.getUserId());
    }

    @Override
    @WebMethod
    public void persistProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "project", partName = "project") @Nullable final Project project
    ) throws IncorrectValueException, ParseException, SessionIsNotValidException {
        if (project == null) throw new IncorrectValueException();
        endpointLocator.getSessionService().validate(session);
        endpointLocator.getProjectService().persist(project);
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDto> findAllProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws SessionIsNotValidException, ListIsEmptyExeption, IncorrectValueException {
        endpointLocator.getSessionService().validate(session);
        return endpointLocator.getProjectService().findAll(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDto> searchByStringProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "string", partName = "string") @Nullable final String string
    ) throws ListIsEmptyExeption, IncorrectValueException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        return endpointLocator.getProjectService().searchByString(session.getUserId(), string);
    }

    @Override
    @WebMethod
    public void removeAllProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws RemoveFailedException, IncorrectValueException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        endpointLocator.getProjectService().removeAll(session.getUserId());
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws RemoveFailedException, IncorrectValueException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        endpointLocator.getProjectService().removeOne(projectId, session.getUserId());
    }

    @Override
    @WebMethod
    public void updateProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description,
            @WebParam(name = "status", partName = "status") @Nullable final String status,
            @WebParam(name = "dateBegin", partName = "dateBegin") @Nullable final String dateBegin,
            @WebParam(name = "dateEnd", partName = "dateEnd") @Nullable final String dateEnd
    ) throws IncorrectValueException, ParseException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        endpointLocator.getProjectService().update(session.getUserId(), projectId, name, description, status, dateBegin, dateEnd);
    }

    @Override
    @WebMethod
    public void mergeProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "project", partName = "project") @Nullable final Project project
    ) throws IncorrectValueException, ParseException, SessionIsNotValidException, ListIsEmptyExeption {
        endpointLocator.getSessionService().validate(session);
        endpointLocator.getProjectService().merge(project);
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDto> findByAddingProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws ListIsEmptyExeption, IncorrectValueException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        return endpointLocator.getProjectService().findAll(session.getUserId());
    }
}
