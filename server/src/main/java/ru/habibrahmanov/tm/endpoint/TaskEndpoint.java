package ru.habibrahmanov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.ITaskEndpoint;
import ru.habibrahmanov.tm.dto.SessionDto;
import ru.habibrahmanov.tm.dto.TaskDto;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RemoveFailedException;
import ru.habibrahmanov.tm.exeption.SessionIsNotValidException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.text.ParseException;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.habibrahmanov.tm.api.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @Override
    @WebMethod
    public void persistTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "task", partName = "task") @Nullable final Task task
    ) throws IncorrectValueException, ParseException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        endpointLocator.getTaskService().persist(task);
    }

    @Override
    @WebMethod
    public void insertTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description,
            @WebParam(name = "dateBegin", partName = "dateBegin") @Nullable final String dateBegin,
            @WebParam(name = "dateEnd", partName = "dateEnd") @Nullable final String dateEnd
    ) throws IncorrectValueException, ParseException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        endpointLocator.getTaskService().insert(projectId, session.getUserId(), name, description, dateBegin, dateEnd);
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDto findOneTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) throws IncorrectValueException, ListIsEmptyExeption, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        return endpointLocator.getTaskService().findOne(taskId, session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDto> findAllTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws ListIsEmptyExeption, IncorrectValueException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        return endpointLocator.getTaskService().findAll(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDto> findAllSortedByDateBeginTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws ListIsEmptyExeption, IncorrectValueException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        return endpointLocator.getTaskService().findAllSortedByDateBegin(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDto> findAllSortedByDateEndTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws ListIsEmptyExeption, IncorrectValueException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        return endpointLocator.getTaskService().findAllSortedByDateEnd(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDto> findAllSortedByStatusTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws IncorrectValueException, SessionIsNotValidException, ListIsEmptyExeption {
        endpointLocator.getSessionService().validate(session);
        return endpointLocator.getTaskService().findAllSortedByStatus(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDto> searchByStringTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "string", partName = "string") @Nullable final String string
    ) throws IncorrectValueException, ListIsEmptyExeption, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        return endpointLocator.getTaskService().searchByString(session.getUserId(), string);
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) throws IncorrectValueException, RemoveFailedException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        endpointLocator.getTaskService().remove(session.getUserId(), taskId);
    }

    @Override
    @WebMethod
    public void removeAllTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws IncorrectValueException, RemoveFailedException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        endpointLocator.getTaskService().removeAll(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void updateTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description,
            @WebParam(name = "status", partName = "status") @Nullable final String status,
            @WebParam(name = "dateBegin", partName = "dateBegin") @Nullable final String dateBegin,
            @WebParam(name = "dateEnd", partName = "dateEnd") @Nullable final String dateEnd
    ) throws IncorrectValueException, ParseException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        endpointLocator.getTaskService().update(projectId, session.getUserId(), id, name, description, status, dateBegin, dateEnd);
    }

    @Override
    @WebMethod
    public void mergeTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "task", partName = "task") @Nullable final Task task
    ) throws IncorrectValueException, ParseException, SessionIsNotValidException {
        endpointLocator.getSessionService().validate(session);
        endpointLocator.getTaskService().merge(task);
    }
}