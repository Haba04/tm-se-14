package ru.habibrahmanov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.IUserEndpoint;
import ru.habibrahmanov.tm.dto.SessionDto;
import ru.habibrahmanov.tm.dto.UserDto;
import ru.habibrahmanov.tm.entity.Session;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.enumeration.Role;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RoleTypeException;
import ru.habibrahmanov.tm.exeption.SessionIsNotValidException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.habibrahmanov.tm.api.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @Override
    @WebMethod
    public void registryAdmin(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "passwordConfirm", partName = "passwordConfirm") @Nullable final String passwordConfirm
    ) throws IllegalArgumentException, UnsupportedEncodingException, NoSuchAlgorithmException, IncorrectValueException {
        endpointLocator.getUserService().registryAdmin(login, password, passwordConfirm);
    }

    @Override
    @WebMethod
    public void registryUser(
            @WebParam(name = "user", partName = "user") @Nullable final User user
    ) throws IllegalArgumentException {
        endpointLocator.getUserService().registryUser(user);
    }

    @Override
    @WebMethod
    public void updatePassword(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "curPassword", partName = "curPassword") @Nullable final String curPassword,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable final String newPassword,
            @WebParam(name = "newPasswordConfirm", partName = "newPasswordConfirm") @Nullable final String newPasswordConfirm
    ) throws UnsupportedEncodingException, NoSuchAlgorithmException, SessionIsNotValidException, IncorrectValueException {
        endpointLocator.getSessionService().validate(session);
        endpointLocator.getUserService().updatePassword(session.getUserId(), curPassword, newPassword, newPasswordConfirm);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDto viewProfile(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws SessionIsNotValidException, IncorrectValueException {
        endpointLocator.getSessionService().validate(session);
        return endpointLocator.getUserService().findOne(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public List<UserDto> findAll(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws ListIsEmptyExeption, SessionIsNotValidException, IncorrectValueException {
        endpointLocator.getSessionService().validate(session);
        if (endpointLocator.getUserService().findOne(session.getUserId()).getRole().equals(Role.ADMIN))
            return endpointLocator.getUserService().findAll();
        return null;
    }

    @Override
    @WebMethod
    public void RemoveAll(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws IncorrectValueException, RoleTypeException {
        if (endpointLocator.getUserService().findOne(session.getUserId()).getRole().equals(Role.ADMIN))
            throw new RoleTypeException();
        endpointLocator.getUserService().RemoveAll();
    }

    @Override
    @WebMethod
    public void editProfile(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "newLogin", partName = "newLogin") @Nullable final String newLogin
    ) throws SessionIsNotValidException, IncorrectValueException {
        endpointLocator.getSessionService().validate(session);
        endpointLocator.getUserService().findOne(session.getUserId()).setLogin(newLogin);
    }
}
