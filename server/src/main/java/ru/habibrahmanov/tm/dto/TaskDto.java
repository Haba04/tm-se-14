package ru.habibrahmanov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.enumeration.Status;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class TaskDto extends AbstractDto implements Comparable<TaskDto>, Serializable {
    @Nullable
    private String projectId;
    @Nullable
    private String userId;
    @Nullable
    private String name;
    @Nullable
    private String description;
    @Nullable
    private Date dateBegin;
    @Nullable
    private Date dateEnd;
    @Nullable
    private Status status = Status.PLANNED;

    public TaskDto(@NotNull final String projectId, @NotNull final String userId, @NotNull final String name,
                   @NotNull final String description, @NotNull final Date dateBegin, @NotNull final Date dateEnd) {
        this.projectId = projectId;
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }

    @Override
    public int compareTo(@NotNull TaskDto o) {
        return this.getStatus().compareTo(o.getStatus());
    }
}
