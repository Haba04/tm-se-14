package ru.habibrahmanov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;

@Getter
@Setter
@NoArgsConstructor
public class SessionDto extends AbstractDto implements Cloneable {
    @Nullable
    private String userId;
    @Nullable
    private Long timestamp = System.currentTimeMillis();
    @Nullable
    private String signature;

    @Override
    public SessionDto clone() {
        try {
            return (SessionDto) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}
