package ru.habibrahmanov.tm.util;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.dto.ProjectDto;
import ru.habibrahmanov.tm.dto.SessionDto;
import ru.habibrahmanov.tm.dto.TaskDto;
import ru.habibrahmanov.tm.dto.UserDto;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.entity.Session;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.service.PropertyService;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public class HibernateUtil {

    @NotNull
    private static final PropertyService propertyService = new PropertyService();

    public static EntityManagerFactory factory() {
        final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getDriver());
        settings.put(Environment.URL, propertyService.getUrl());
        settings.put(Environment.USER, propertyService.getDbLogin());
        settings.put(Environment.PASS, propertyService.getDbPassword());
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");
        final StandardServiceRegistryBuilder registryBuilder
                = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(TaskDto.class);
        sources.addAnnotatedClass(ProjectDto.class);
        sources.addAnnotatedClass(UserDto.class);
        sources.addAnnotatedClass(SessionDto.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }
}
