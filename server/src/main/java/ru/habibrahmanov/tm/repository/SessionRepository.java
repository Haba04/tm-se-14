package ru.habibrahmanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.api.ISessionRepository;
import ru.habibrahmanov.tm.dto.SessionDto;
import ru.habibrahmanov.tm.entity.Session;

import javax.persistence.EntityManager;

public class SessionRepository implements ISessionRepository {

    @NotNull
    private final EntityManager entityManager;

    public SessionRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void persist(@NotNull final Session session) {
        entityManager.persist(session);
    }

    @Override
    public void remove(@NotNull final Session session) {
        @NotNull final String query = "DELETE FROM Session WHERE id = :id";
        entityManager.createQuery(query).setParameter("id", session.getId()).executeUpdate();
    }
}
