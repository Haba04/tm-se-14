package ru.habibrahmanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.IProjectRepository;
import ru.habibrahmanov.tm.entity.Project;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    @NotNull
    private final EntityManager entityManager;

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void persist(@NotNull Project project) {
        entityManager.persist(project);
    }

    @Override
    @Nullable
    public Project findOne(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String query = "SELECT project FROM Project project WHERE project.user.id = :userId AND project.id = :id";
        @NotNull final Project project = entityManager.createQuery(query, Project.class)
                .setParameter("userId", userId).setParameter("id", id).getSingleResult();
        return project;
    }

    @Override
    @Nullable
    public List<Project> findAllByUserId(@NotNull final String userId) {
        @NotNull final String query = "SELECT project FROM Project project WHERE project.user.id = :userId";
        @NotNull final List<Project> projectList = entityManager.createQuery(query, Project.class)
                .setParameter("userId", userId).getResultList();
        return projectList;
    }

    @Override
    @Nullable
    public List<Project> findAll() {
        @NotNull final String query = "SELECT project FROM Project project";
        @NotNull final List<Project> projectList = entityManager.createQuery(query, Project.class).getResultList();
        return projectList;
    }

    @Override
    public void removeOne(@NotNull final String id, @NotNull final String userId) {
        @NotNull final String query = "DELETE FROM Project WHERE user.id = :userId AND id = :id";
        entityManager.createQuery(query).setParameter("userId", userId).setParameter("id", id).executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final String query = "DELETE FROM Project WHERE user.id = :userId";
        entityManager.createQuery(query).setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void update(@NotNull final Project project) {
        @NotNull final String query = "UPDATE Project SET name = :name, description = :description, " +
                "dateBegin = :dateBegin, dateEnd = :dateEnd, status = :status WHERE id = :projectId";
        entityManager.createQuery(query)
                .setParameter("name", project.getName()).setParameter("description", project.getDescription())
                .setParameter("dateBegin", project.getDateBegin()).setParameter("dateEnd", project.getDateEnd())
                .setParameter("status", project.getStatus()).setParameter("projectId", project.getId()).executeUpdate();
    }

    @Override
    @Nullable
    public List<Project> getProjectsByString(@NotNull final String userId, @NotNull final String string) {
        @NotNull final String query = "SELECT project FROM Project project WHERE project.user.id = :userId AND project.name LIKE :string OR project.description LIKE :string";
        @Nullable final List<Project> projectList = entityManager.createQuery(query, Project.class).setParameter("userId", userId).setParameter("string", string).getResultList();
        return projectList;
    }
}