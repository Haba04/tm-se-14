package ru.habibrahmanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.IUserRepository;
import ru.habibrahmanov.tm.entity.User;
import javax.persistence.EntityManager;
import java.util.List;

public class UserRepository implements IUserRepository {

    @NotNull
    private final EntityManager entityManager;

    public UserRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void persist(@NotNull final User user) {
        entityManager.persist(user);
    }

    @Override
    @NotNull
    public User findOne(@NotNull final String userId) {
        @NotNull final String query = "SELECT user FROM User user WHERE user.id = :userId";
        @NotNull final User user = entityManager.createQuery(query, User.class).setParameter("userId", userId).getSingleResult();
        return user;
    }

    @Override
    @NotNull
    public User findByLogin(@NotNull final String login) {
        @NotNull final String query = "SELECT user FROM User user WHERE user.login = :login";
        @NotNull final User user = entityManager.createQuery(query, User.class).setParameter("login", login).getSingleResult();
        return user;
    }

    @Override
    @Nullable
    public List<User> findAll() {
        @NotNull final String query = "SELECT user FROM User user";
        @NotNull final List<User> userList = entityManager.createQuery(query, User.class).getResultList();
        return userList;
    }

    @Override
    public void removeOne(@NotNull final String userId) {
        @NotNull final String query = "DELETE FROM User WHERE id = :userId";
        entityManager.createQuery(query).setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void removeAll() {
        @NotNull final String query = "DELETE FROM User";
        entityManager.createQuery(query).executeUpdate();
    }

    @Override
    public void update(@NotNull User user) {
        @NotNull final String query = "UPDATE User SET login = :login WHERE id = :userId";
        entityManager.createQuery(query).setParameter("login", user.getLogin()).setParameter("userId", user.getId()).executeUpdate();
    }
}