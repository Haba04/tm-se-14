package ru.habibrahmanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.api.ITaskRepository;
import ru.habibrahmanov.tm.entity.Task;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    @NotNull private EntityManager entityManager;

    public TaskRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void persist(@NotNull final Task task) {
        entityManager.persist(task);
    }

    @Override
    @NotNull
    public Task findOne(@NotNull final String userId, @NotNull final String taskId) {
        @NotNull final String query = "SELECT task FROM Task task WHERE task.id = :taskId AND task.user.id = :userId";
        @NotNull Task task = entityManager.createQuery(query, Task.class).setParameter("taskId", taskId)
                .setParameter("userId", userId).getSingleResult();
        return task;
    }

    @Override
    @NotNull
    public List<Task> findAllByUserId(@NotNull final String userId) {
        @NotNull final String query = "SELECT task FROM Task task WHERE task.user.id = :userId";
        @NotNull List<Task> taskList = entityManager.createQuery(query, Task.class).setParameter("userId", userId).getResultList();
        return taskList;
    }

    @Override
    @NotNull
    public List<Task> findAll() {
        @NotNull final String query = "SELECT task FROM Task task";
        @NotNull List<Task> taskList = entityManager.createQuery(query, Task.class).getResultList();
        return taskList;
    }

    @Override
    @NotNull
    public List<Task> searchByString(@NotNull final String userId, @NotNull final String string) {
        @NotNull final String query = "SELECT task FROM Task task WHERE task.user.id = :userId AND task.name LIKE :string OR task.name LIKE :string";
        @NotNull List<Task> taskList = entityManager.createQuery(query, Task.class)
                .setParameter("userId", userId).setParameter("string", string).getResultList();
        return taskList;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String taskId) {
        @NotNull final String query = "DELETE FROM Task WHERE id = :taskId AND user.id = :userId";
        entityManager.createQuery(query).setParameter("userId", userId).setParameter("taskId", taskId).executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String query = "DELETE FROM Task WHERE project.id = :projectId AND user.id = :userId";
        entityManager.createQuery(query).setParameter("userId", userId).setParameter("projectId", projectId).executeUpdate();
    }

    @Override
    public void update(@NotNull final Task task) {
        @NotNull final String query = "UPDATE Task SET name = :name, description = :description, " +
                "dateBegin = :dateBegin, dateEnd = :dateEnd, status = :status WHERE id = :taskId";
        entityManager.createQuery(query)
                .setParameter("name", task.getName()).setParameter("description", task.getDescription())
                .setParameter("dateBegin", task.getDateBegin()).setParameter("dateEnd", task.getDateEnd())
                .setParameter("status", task.getStatus()).setParameter("taskId", task.getId()).executeUpdate();
    }
}
