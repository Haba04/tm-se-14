package ru.habibrahmanov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.habibrahmanov.tm.api.*;
import ru.habibrahmanov.tm.endpoint.*;
import ru.habibrahmanov.tm.service.ProjectService;
import ru.habibrahmanov.tm.service.SessionService;
import ru.habibrahmanov.tm.service.TaskService;
import ru.habibrahmanov.tm.service.UserService;
import ru.habibrahmanov.tm.util.PublishUtil;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

public final class Bootstrap implements EndpointLocator {
    @NotNull private final IProjectService projectService = new ProjectService();
    @NotNull private final ITaskService taskService = new TaskService();
    @NotNull private final IUserService userService = new UserService();
    @NotNull private final ISessionService sessionService = new SessionService();
    @NotNull private final IProjectEndpoint projectEndpoint = new ProjectEndpoint();
    @NotNull private final ITaskEndpoint taskEndpoint = new TaskEndpoint();
    @NotNull private final IUserEndpoint userEndpoint = new UserEndpoint();
    @NotNull private final ISessionEndpoint sessionEndpoint = new SessionEndpoint();
    @NotNull private final Scanner scanner = new Scanner(System.in);

    private final Set<AbstractEndpoint> endpoits = new LinkedHashSet<>();
    private final Set<Class<? extends AbstractEndpoint>> endpointClasses = new Reflections("ru.habibrahmanov.tm.endpoint")
            .getSubTypesOf(AbstractEndpoint.class);


    public void init() throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        registry(endpointClasses);
        PublishUtil.publish(endpoits);
    }

    public void registry(
            @NotNull final Set<Class<? extends AbstractEndpoint>> classes
    ) throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        for (@NotNull final Class clazz: classes) {
            registry(clazz);
        }
    }

    public void registry(
            @NotNull final Class clazz
    ) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        @NotNull final AbstractEndpoint abstractEndpoint = (AbstractEndpoint) clazz.getDeclaredConstructor().newInstance();
        abstractEndpoint.setEndpointLocator(this);
        endpoits.add(abstractEndpoint);
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public Scanner getScanner() {
        return scanner;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @Override
    public ITaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @Override
    public IUserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    @Override
    public ISessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }
}