package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {
    void persist(@NotNull Task task);

    @NotNull Task findOne(@NotNull String userId, @NotNull String taskId);

    @NotNull List<Task> findAllByUserId(@NotNull String userId);

    @NotNull List<Task> findAll();

    @NotNull List<Task> searchByString(@NotNull String userId, @NotNull String string);

    void remove(@NotNull String userId, @NotNull String taskId);

    void removeAll(@NotNull String userId, @NotNull String projectId);

    void update(@NotNull Task task);
}
