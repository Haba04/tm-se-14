package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.dto.UserDto;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface IUserService {
    void registryAdmin(
            @Nullable String login, @Nullable String password, @Nullable String passwordConfirm
    ) throws IllegalArgumentException, UnsupportedEncodingException, NoSuchAlgorithmException, IncorrectValueException;

    void registryUser(@Nullable User user) throws IllegalArgumentException;

    void updatePassword(
            @Nullable String userId, @Nullable String curPassword, @Nullable String newPassword,
            @Nullable String newPasswordConfirm
    ) throws UnsupportedEncodingException, NoSuchAlgorithmException, IncorrectValueException;

    @NotNull List<UserDto> findAll() throws ListIsEmptyExeption;

    void RemoveAll();

    @NotNull UserDto findOne(@Nullable String userId) throws IncorrectValueException;

    @NotNull UserDto findByLogin(@Nullable String login) throws ListIsEmptyExeption;

    UserDto convertEntityToDto(User user);

    User convertDtoToEntity(UserDto userDto);
}
