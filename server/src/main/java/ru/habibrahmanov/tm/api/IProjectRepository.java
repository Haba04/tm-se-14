package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {
    void persist(@NotNull Project project);

    @Nullable Project findOne(@NotNull String userId, @NotNull String id);

    @Nullable List<Project> findAllByUserId(@NotNull String userId);

    @Nullable List<Project> findAll();

    void removeOne(@NotNull String id, @NotNull String userId);

    void removeAll(@NotNull String userId);

    void update(@NotNull Project project);

    @Nullable List<Project> getProjectsByString(@NotNull String userId, @NotNull String string);
}
