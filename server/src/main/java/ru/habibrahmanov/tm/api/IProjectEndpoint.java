package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.dto.ProjectDto;
import ru.habibrahmanov.tm.dto.SessionDto;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RemoveFailedException;
import ru.habibrahmanov.tm.exeption.SessionIsNotValidException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.text.ParseException;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    String URL = "http://localhost:8080/ProjectEndpoint?wsdl";


    @WebMethod
    void insertProject(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @Nullable String description,
            @WebParam(name = "dateBegin", partName = "dateBegin") @Nullable String dateBegin,
            @WebParam(name = "dateEnd", partName = "dateEnd") @Nullable String dateEnd
    ) throws ParseException, IncorrectValueException, SessionIsNotValidException;

    @WebMethod
    void persistProject(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session,
            @WebParam(name = "project", partName = "project") @Nullable Project project
    ) throws IncorrectValueException, ParseException, SessionIsNotValidException;

    @NotNull
    @WebMethod
    List<ProjectDto> findAllProject(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session
    ) throws SessionIsNotValidException, ListIsEmptyExeption, IncorrectValueException;

    @NotNull
    @WebMethod
    List<ProjectDto> searchByStringProject(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session,
            @WebParam(name = "string", partName = "string") @Nullable String string
    ) throws ListIsEmptyExeption, IncorrectValueException, SessionIsNotValidException;

    @WebMethod
    void removeAllProject(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session
    ) throws RemoveFailedException, IncorrectValueException, SessionIsNotValidException;

    @WebMethod
    void removeProject(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId
    ) throws RemoveFailedException, IncorrectValueException, SessionIsNotValidException;

    @WebMethod
    void updateProject(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @Nullable String description,
            @WebParam(name = "status", partName = "status") @Nullable String status,
            @WebParam(name = "dateBegin", partName = "dateBegin") @Nullable String dateBegin,
            @WebParam(name = "dateEnd", partName = "dateEnd") @Nullable String dateEnd
    ) throws IncorrectValueException, ParseException, SessionIsNotValidException;

    @WebMethod
    void mergeProject(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session,
            @WebParam(name = "project", partName = "project") @Nullable Project project
    ) throws IncorrectValueException, ParseException, SessionIsNotValidException, ListIsEmptyExeption;

    @NotNull
    @WebMethod
    List<ProjectDto> findByAddingProject(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session
    ) throws ListIsEmptyExeption, IncorrectValueException, SessionIsNotValidException;
}
