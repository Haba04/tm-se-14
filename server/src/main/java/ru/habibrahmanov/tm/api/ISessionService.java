package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.dto.SessionDto;
import ru.habibrahmanov.tm.entity.Session;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.SessionIsNotValidException;


public interface ISessionService {
    void validate(@Nullable SessionDto session) throws SessionIsNotValidException;

    @Nullable SessionDto open(@Nullable String login, @Nullable String password) throws IncorrectValueException;

    void close(@Nullable SessionDto session) throws SessionIsNotValidException;
}
