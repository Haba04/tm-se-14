package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.entity.Session;

public interface ISessionRepository {
    void persist(@NotNull Session session);

    void remove(@NotNull Session session);
}
