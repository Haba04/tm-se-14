package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.User;

import java.util.List;

public interface IUserRepository {
    void persist(@NotNull User user);

    @NotNull User findOne(@NotNull String userId);

    @NotNull User findByLogin(@NotNull String login);

    @Nullable List<User> findAll();

    void removeOne(@NotNull String userId);

    void removeAll();

    void update(@NotNull User user);
}
